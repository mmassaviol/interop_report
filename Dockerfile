FROM mbbteam/mbb_workflows_base:latest as alltools

ENV PATH /opt/biotools/InterOp-1.1.8-Linux-GNU/bin/:$PATH 
RUN cd /opt/biotools \
 && wget https://github.com/Illumina/interop/releases/download/v1.1.8/InterOp-1.1.8-Linux-GNU.tar.gz \
 && tar -xvzf InterOp-1.1.8-Linux-GNU.tar.gz \
 && rm InterOp-1.1.8-Linux-GNU.tar.gz

RUN apt -y update && apt install -y gnuplot

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

#This part is necessary to run on ISEM cluster
RUN mkdir -p /share/apps/bin \
 && mkdir -p /share/apps/lib \
 && mkdir -p /share/apps/gridengine \
 && mkdir -p /share/bio \
 && mkdir -p /opt/gridengine \
 && mkdir -p /export/scrach \
 && mkdir -p /usr/lib64 \
 && ln -s /bin/bash /bin/mbb_bash \
 && ln -s /bin/bash /bin/isem_bash \
 && /usr/sbin/groupadd --system --gid 400 sge \
 && /usr/sbin/useradd --system --uid 400 --gid 400 -c GridEngine --shell /bin/true --home /opt/gridengine sge

EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

